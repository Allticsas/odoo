# -*- coding: utf-8 -*-
{
    'name': "Alltic",

    'summary': """
        Módulo personalizado para cambiar modelos""",

    'description': """

    """,

    'author': "Alltic",
    'website': "http://www.alltic.co",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'hr',
    'version': '0.4',
    'installable': True,
    'auto_install': False,
    # any module necessary for this one to work correctly
    'depends': ['base','hr','hr_applicant']

}
